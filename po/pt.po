# Portuguese translation for redshift
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the redshift package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: redshift\n"
"Report-Msgid-Bugs-To: https://github.com/jonls/redshift/issues\n"
"POT-Creation-Date: 2020-09-04 12:22-0700\n"
"PO-Revision-Date: 2016-05-21 16:23+0000\n"
"Last-Translator: Pedro Beja <althaser@gmail.com>\n"
"Language-Team: Portuguese <pt@li.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2018-05-21 01:04+0000\n"
"X-Generator: Launchpad (build 18658)\n"

#: ../data/appdata/gammastep-indicator.appdata.xml.in.h:1
msgid ""
"Adjusts the color temperature of your screen according to your surroundings. "
"This may help your eyes hurt less if you are working in front of the screen "
"at night."
msgstr ""

#: ../data/appdata/gammastep-indicator.appdata.xml.in.h:2
msgid ""
"The color temperature is set according to the position of the sun. A "
"different color temperature is set during night and daytime. During twilight "
"and early morning, the color temperature transitions smoothly from night to "
"daytime temperature to allow your eyes to slowly adapt."
msgstr ""

#: ../data/appdata/gammastep-indicator.appdata.xml.in.h:3
msgid ""
"This program provides a status icon that allows the user to control color "
"temperature."
msgstr ""

#: ../data/applications/gammastep.desktop.in.h:1
msgid "gammastep"
msgstr ""

#: ../data/applications/gammastep.desktop.in.h:2
#, fuzzy
msgid "Color temperature adjustment"
msgstr "Temperatura da cor"

#: ../data/applications/gammastep.desktop.in.h:3
#, fuzzy
msgid "Color temperature adjustment tool"
msgstr "O ajustamento da temperatura falhou.\n"

#: ../data/applications/gammastep-indicator.desktop.in.h:1
msgid "Gammastep Indicator"
msgstr ""

#: ../data/applications/gammastep-indicator.desktop.in.h:2
#, fuzzy
msgid "Indicator for color temperature adjustment"
msgstr "Argumento da temperatura malformado.\n"

#. TRANSLATORS: Name printed when period of day is unknown
#: ../src/redshift.c:92
msgid "None"
msgstr "Nenhum"

#: ../src/redshift.c:93
msgid "Daytime"
msgstr "Diurno"

#: ../src/redshift.c:94 ../src/redshift.c:958 ../src/redshift.c:965
#: ../src/redshift.c:1022
msgid "Night"
msgstr "Noite"

#: ../src/redshift.c:95
msgid "Transition"
msgstr "Transição"

#: ../src/redshift.c:178 ../src/gammastep_indicator/statusicon.py:313
#: ../src/gammastep_indicator/statusicon.py:326
msgid "Period"
msgstr "Período"

#: ../src/redshift.c:188 ../src/redshift.c:958 ../src/redshift.c:965
#: ../src/redshift.c:1018
#, fuzzy
msgid "Day"
msgstr "Diurno"

#. TRANSLATORS: Abbreviation for `north'
#: ../src/redshift.c:198
msgid "N"
msgstr "N"

#. TRANSLATORS: Abbreviation for `south'
#: ../src/redshift.c:200
msgid "S"
msgstr "S"

#. TRANSLATORS: Abbreviation for `east'
#: ../src/redshift.c:202
msgid "E"
msgstr "E"

#. TRANSLATORS: Abbreviation for `west'
#: ../src/redshift.c:204
msgid "W"
msgstr "O"

#: ../src/redshift.c:206 ../src/gammastep_indicator/statusicon.py:319
msgid "Location"
msgstr "Localização"

#: ../src/redshift.c:280
#, fuzzy
msgid "Failed to initialize provider"
msgstr "Falhou a iniciar o fornecedor %s.\n"

#: ../src/redshift.c:294 ../src/redshift.c:334 ../src/redshift.c:378
#: ../src/redshift.c:404
#, fuzzy
msgid "Failed to set option"
msgstr "Falhou a definir %s opção.\n"

#: ../src/redshift.c:297 ../src/redshift.c:336
msgid "For more information, use option:"
msgstr ""

#: ../src/redshift.c:324 ../src/redshift.c:395
#, fuzzy
msgid "Failed to parse option"
msgstr "Falhou a analisar a opção '%s'.\n"

#: ../src/redshift.c:349
#, fuzzy
msgid "Failed to start provider"
msgstr "Falhou a iniciar o fornecedor %s.\n"

#: ../src/redshift.c:364
#, fuzzy
msgid "Failed to initialize method"
msgstr "Falhou ao iniciar o método de ajuste %s.\n"

#: ../src/redshift.c:379 ../src/redshift.c:405
#, fuzzy
msgid "For more information, try:"
msgstr "Tente `-h' para mais informação.\n"

#: ../src/redshift.c:417
#, fuzzy
msgid "Failed to start adjustment method"
msgstr "Falhou ao iniciar o método de ajuste %s.\n"

#: ../src/redshift.c:445
#, fuzzy
msgid "Latitude must be in range"
msgstr "A latitude deve estar entre %.1f e %.1f.\n"

#: ../src/redshift.c:452
#, fuzzy
msgid "Longitude must be in range:"
msgstr "A longitude deve estar entre %.1f e %.1f.\n"

#: ../src/redshift.c:479 ../src/redshift.c:497 ../src/redshift.c:624
#: ../src/redshift.c:1096
#, fuzzy
msgid "Unable to read system time."
msgstr "Incapaz de ler as horas do sistema.\n"

#: ../src/redshift.c:567
msgid "Waiting for initial location to become available..."
msgstr ""

#: ../src/redshift.c:572 ../src/redshift.c:763 ../src/redshift.c:777
#: ../src/redshift.c:1081
#, fuzzy
msgid "Unable to get location from provider."
msgstr "Incapaz de adquirir a localização do fornecedor.\n"

#: ../src/redshift.c:577 ../src/redshift.c:800
#, fuzzy
msgid "Invalid location returned from provider."
msgstr "Incapaz de adquirir a localização do fornecedor.\n"

#: ../src/redshift.c:584 ../src/redshift.c:715 ../src/redshift.c:1130
#: ../src/redshift.c:1155 ../src/gammastep_indicator/statusicon.py:307
#: ../src/gammastep_indicator/statusicon.py:325
msgid "Color temperature"
msgstr "Temperatura da cor"

#: ../src/redshift.c:585 ../src/redshift.c:718 ../src/redshift.c:1003
#: ../src/redshift.c:1131
#, fuzzy
msgid "Brightness"
msgstr "Brilho: %.2f\n"

#: ../src/redshift.c:614
msgid "Status"
msgstr ""

#: ../src/redshift.c:614 ../src/gammastep_indicator/statusicon.py:302
msgid "Disabled"
msgstr "Desativado"

#: ../src/redshift.c:614 ../src/gammastep_indicator/statusicon.py:61
#: ../src/gammastep_indicator/statusicon.py:302
msgid "Enabled"
msgstr "Ativado"

#: ../src/redshift.c:727 ../src/redshift.c:1139 ../src/redshift.c:1163
#: ../src/redshift.c:1184
#, fuzzy
msgid "Temperature adjustment failed."
msgstr "O ajustamento da temperatura falhou.\n"

#: ../src/redshift.c:783
msgid ""
"Location is temporarily unavailable; Using previous location until it "
"becomes available..."
msgstr ""

#: ../src/redshift.c:890
msgid "Partial time-configuration unsupported!"
msgstr ""

#: ../src/redshift.c:897
msgid "Invalid dawn/dusk time configuration!"
msgstr ""

#: ../src/redshift.c:926
#, fuzzy
msgid "Trying location provider"
msgstr "A tentar a localização do fornecedor '%s'...\n"

#: ../src/redshift.c:931
#, fuzzy
msgid "Trying next provider..."
msgstr "A tentar o fornecedor seguinte...\n"

#: ../src/redshift.c:936
#, fuzzy
msgid "Using provider:"
msgstr "A utilizar o fornecedor `%s'.\n"

#: ../src/redshift.c:944
#, fuzzy
msgid "No more location providers to try."
msgstr "Não existem mais fornecedores de localização para tentar.\n"

#: ../src/redshift.c:952
#, fuzzy
msgid ""
"High transition elevation cannot be lower than the low transition elevation."
msgstr ""
"A elevação de alta transição não pode ser menor que a elevação de baixa "
"transição.\n"

#: ../src/redshift.c:958
#, fuzzy
msgid "Solar elevations"
msgstr "Elevação solar: %f\n"

#: ../src/redshift.c:965
#, fuzzy
msgid "Temperatures"
msgstr "Temperatura da cor"

#: ../src/redshift.c:975 ../src/redshift.c:986
#, fuzzy
msgid "Temperature must be in range"
msgstr "A latitude deve estar entre %.1f e %.1f.\n"

#: ../src/redshift.c:997
#, fuzzy
msgid "Brightness must be in range"
msgstr "A latitude deve estar entre %.1f e %.1f.\n"

#: ../src/redshift.c:1010
#, fuzzy
msgid "Gamma value must be in range"
msgstr "A latitude deve estar entre %.1f e %.1f.\n"

#: ../src/redshift.c:1018 ../src/redshift.c:1022
msgid "Gamma"
msgstr ""

#: ../src/redshift.c:1049
#, fuzzy
msgid "Trying next method..."
msgstr "A tentar o próximo método...\n"

#: ../src/redshift.c:1054
#, fuzzy
msgid "Using method"
msgstr "A usar o método '%s'.\n"

#: ../src/redshift.c:1061
#, fuzzy
msgid "No more methods to try."
msgstr "Não há mais métodos para tentar.\n"

#: ../src/redshift.c:1075
msgid "Waiting for current location to become available..."
msgstr ""

#: ../src/redshift.c:1086
#, fuzzy
msgid "Invalid location from provider."
msgstr "Incapaz de adquirir a localização do fornecedor.\n"

#: ../src/redshift.c:1112
#, fuzzy
msgid "Solar elevation"
msgstr "Elevação solar: %f\n"

#: ../src/redshift.c:1147 ../src/redshift.c:1171 ../src/redshift.c:1192
msgid "Press ctrl-c to stop..."
msgstr ""

#. TRANSLATORS: help output 1
#. LAT is latitude, LON is longitude,
#. DAY is temperature at daytime,
#. NIGHT is temperature at night
#. no-wrap
#: ../src/options.c:144
#, c-format
msgid "Usage: %s -l LAT:LON -t DAY:NIGHT [OPTIONS...]\n"
msgstr "Uso: %s -l LAT:LON -t DIA:NOITE [OPÇÕES...]\n"

#. TRANSLATORS: help output 2
#. no-wrap
#: ../src/options.c:150
msgid "Set color temperature of display according to time of day.\n"
msgstr "Definir temperatura da cor do ecrã de acordo com a hora do dia.\n"

#. TRANSLATORS: help output 3
#. no-wrap
#: ../src/options.c:156
#, fuzzy
msgid ""
"  -h\t\tDisplay this help message\n"
"  -v\t\tIncrease logging verbosity\n"
"  -q\t\tDecrease logging verbosity\n"
"  -V\t\tShow program version\n"
msgstr ""
"  -h\t\tMostrar esta mensagem de ajuda\n"
"  -v\t\tVista detalhada\n"
"  -V\t\tMostrar versão do programa\n"

#. TRANSLATORS: help output 4
#. `list' must not be translated
#. no-wrap
#: ../src/options.c:165
msgid ""
"  -b DAY:NIGHT\tScreen brightness to apply (between 0.1 and 1.0)\n"
"  -c FILE\tLoad settings from specified configuration file\n"
"  -g R:G:B\tAdditional gamma correction to apply\n"
"  -l LAT:LON\tYour current location\n"
"  -l PROVIDER\tSelect provider for automatic location updates\n"
"  \t\t(Type `list' to see available providers)\n"
"  -m METHOD\tMethod to use to set color temperature\n"
"  \t\t(Type `list' to see available methods)\n"
"  -o\t\tOne shot mode (do not continuously adjust color temperature)\n"
"  -O TEMP\tOne shot manual mode (set color temperature)\n"
"  -p\t\tPrint mode (only print parameters and exit)\n"
"  -P\t\tReset existing gamma ramps before applying new color effect\n"
"  -x\t\tReset mode (remove adjustment from screen)\n"
"  -r\t\tDisable fading between color temperatures\n"
"  -t DAY:NIGHT\tColor temperature to set at daytime/night\n"
msgstr ""

#. TRANSLATORS: help output 5
#: ../src/options.c:187
#, c-format
msgid ""
"The neutral temperature is %uK. Using this value will not change the color\n"
"temperature of the display. Setting the color temperature to a value higher\n"
"than this results in more blue light, and setting a lower value will result "
"in\n"
"more red light.\n"
msgstr ""

#. TRANSLATORS: help output 6
#: ../src/options.c:196
#, c-format
msgid ""
"Default values:\n"
"\n"
"  Daytime temperature: %uK\n"
"  Night temperature: %uK\n"
msgstr ""
"Valores por omissão:\n"
"\n"
"  Temperatura diurna: %uK\n"
"  Temperatura noturna: %uK\n"

#. TRANSLATORS: help output 7
#: ../src/options.c:204
#, c-format
msgid "Please report bugs to <%s>\n"
msgstr "Por favor relate os erros para <%s>\n"

#: ../src/options.c:211
msgid "Available adjustment methods:\n"
msgstr "Métodos de ajuste disponíveis:\n"

#: ../src/options.c:217
msgid "Specify colon-separated options with `-m METHOD:OPTIONS'.\n"
msgstr ""
"Especifique as opções separadas por dois-pontos com `-m MÉTODO:OPÇÕES'.\n"

#. TRANSLATORS: `help' must not be translated.
#: ../src/options.c:220
msgid "Try `-m METHOD:help' for help.\n"
msgstr "Tente `-m MÉTODO:help' para ajuda.\n"

#: ../src/options.c:227
msgid "Available location providers:\n"
msgstr "Fornecedores de localização disponíveis:\n"

#: ../src/options.c:233
msgid "Specify colon-separated options with`-l PROVIDER:OPTIONS'.\n"
msgstr ""
"Especifique opções separadas por dois-pontos com`-l FORNECEDOR:OPÇÕES'.\n"

#. TRANSLATORS: `help' must not be translated.
#: ../src/options.c:236
msgid "Try `-l PROVIDER:help' for help.\n"
msgstr "Tente `-l FORNECEDOR:help' para ajuda.\n"

#: ../src/options.c:337
msgid "Malformed gamma argument.\n"
msgstr "Argumento gama malformado.\n"

#: ../src/options.c:338 ../src/options.c:450 ../src/options.c:473
msgid "Try `-h' for more information.\n"
msgstr "Tente `-h' para mais informação.\n"

#: ../src/options.c:387 ../src/options.c:586
#, fuzzy
msgid "Unknown location provider"
msgstr "Fornecedor de localização desconhecido `%s'.\n"

#: ../src/options.c:419 ../src/options.c:576
#, fuzzy
msgid "Unknown adjustment method"
msgstr "Método de ajuste desconhecido `%s'.\n"

#: ../src/options.c:449
msgid "Malformed temperature argument.\n"
msgstr "Argumento da temperatura malformado.\n"

#: ../src/options.c:545 ../src/options.c:557 ../src/options.c:566
msgid "Malformed gamma setting.\n"
msgstr "Configuração gama malformada.\n"

#: ../src/options.c:596
#, fuzzy
msgid "Malformed dawn-time setting"
msgstr "Configuração gama malformada.\n"

#: ../src/options.c:606
#, fuzzy
msgid "Malformed dusk-time setting"
msgstr "Configuração gama malformada.\n"

#: ../src/options.c:612
#, fuzzy
msgid "Unknown configuration setting"
msgstr "Definições de configuração desconhecidas `%s'.\n"

#: ../src/config-ini.c:143
msgid "Malformed section header in config file.\n"
msgstr "Cabeçalho da secção malformado no ficheiro de configuração.\n"

#: ../src/config-ini.c:179
msgid "Malformed assignment in config file.\n"
msgstr "Atribuição malformada no ficheiro de configuração.\n"

#: ../src/config-ini.c:190
msgid "Assignment outside section in config file.\n"
msgstr "Atribuição fora da secção no ficheiro de configuração.\n"

#: ../src/gamma-drm.c:86
#, c-format
msgid "Failed to open DRM device: %s\n"
msgstr ""

#: ../src/gamma-drm.c:94
#, c-format
msgid "Failed to get DRM mode resources\n"
msgstr "Falhou a obter recursos do modo DRM\n"

#: ../src/gamma-drm.c:104 ../src/gamma-randr.c:373
#, c-format
msgid "CRTC %d does not exist. "
msgstr "CRTC %d não existe. "

#: ../src/gamma-drm.c:107 ../src/gamma-randr.c:376
#, c-format
msgid "Valid CRTCs are [0-%d].\n"
msgstr "CRTCs válidos são [0-%d].\n"

#: ../src/gamma-drm.c:110 ../src/gamma-randr.c:379
#, c-format
msgid "Only CRTC 0 exists.\n"
msgstr "Apenas existe CRTC 0.\n"

#: ../src/gamma-drm.c:148
#, c-format
msgid "CRTC %i lost, skipping\n"
msgstr "CRTC %i perdido, saltando\n"

#: ../src/gamma-drm.c:154
#, c-format
msgid ""
"Could not get gamma ramp size for CRTC %i\n"
"on graphics card %i, ignoring device.\n"
msgstr ""
"Não consegue obter o tamanho do declive gama para CRTC %i\n"
"na placa gráfica %i, ignorando o dispositivo.\n"

#: ../src/gamma-drm.c:167
#, c-format
msgid ""
"DRM could not read gamma ramps on CRTC %i on\n"
"graphics card %i, ignoring device.\n"
msgstr ""
"DRM não consegue ler o declive gama no CRTC %i da\n"
"placa gráfica %i, ignorando o dispositivo.\n"

#: ../src/gamma-drm.c:231
msgid "Adjust gamma ramps with Direct Rendering Manager.\n"
msgstr "Ajusta o declive gama com o Gestor de Rendering Direto.\n"

#. TRANSLATORS: DRM help output
#. left column must not be translated
#: ../src/gamma-drm.c:236
msgid ""
"  card=N\tGraphics card to apply adjustments to\n"
"  crtc=N\tCRTC to apply adjustments to\n"
msgstr ""
"  card=N\tPlaca gráfica a aplicar os ajuste a\n"
"  crtc=N\tCRTC a aplicar os ajustes a\n"

#: ../src/gamma-drm.c:249
#, c-format
msgid "CRTC must be a non-negative integer\n"
msgstr "CRTC deve ser um inteiro não negativo\n"

#: ../src/gamma-drm.c:253 ../src/gamma-randr.c:358 ../src/gamma-vidmode.c:150
#: ../src/gamma-dummy.c:56
#, c-format
msgid "Unknown method parameter: `%s'.\n"
msgstr "Parâmetro do método desconhecido: '%s'.\n"

#: ../src/gamma-wl.c:70
msgid "Not authorized to bind the wlroots gamma control manager interface."
msgstr ""

#: ../src/gamma-wl.c:90
#, fuzzy
msgid "Failed to allocate memory"
msgstr "Falhou ao iniciar o método de ajuste %s.\n"

#: ../src/gamma-wl.c:124
msgid "The zwlr_gamma_control_manager_v1 was removed"
msgstr ""

#: ../src/gamma-wl.c:177
msgid "Could not connect to wayland display, exiting."
msgstr ""

#: ../src/gamma-wl.c:218
msgid "Ignoring Wayland connection error while waiting to disconnect"
msgstr ""

#: ../src/gamma-wl.c:247
#, fuzzy
msgid "Adjust gamma ramps with a Wayland compositor.\n"
msgstr "Ajustar os declives gama com o Windows GDI.\n"

#: ../src/gamma-wl.c:282
msgid "Wayland connection experienced a fatal error"
msgstr ""

#: ../src/gamma-wl.c:347
msgid "Zero outputs support gamma adjustment."
msgstr ""

#: ../src/gamma-wl.c:352
msgid "output(s) do not support gamma adjustment"
msgstr ""

#: ../src/gamma-randr.c:83 ../src/gamma-randr.c:142 ../src/gamma-randr.c:181
#: ../src/gamma-randr.c:207 ../src/gamma-randr.c:264 ../src/gamma-randr.c:424
#, c-format
msgid "`%s' returned error %d\n"
msgstr "'%s' devolveu o erro %d\n"

#: ../src/gamma-randr.c:92
#, c-format
msgid "Unsupported RANDR version (%u.%u)\n"
msgstr "Versão RANDR (%u.%u) não suportada.\n"

#: ../src/gamma-randr.c:127
#, c-format
msgid "Screen %i could not be found.\n"
msgstr "Ecrã %i não consegue ser encontrado.\n"

#: ../src/gamma-randr.c:193 ../src/gamma-vidmode.c:85
#, c-format
msgid "Gamma ramp size too small: %i\n"
msgstr "Tamanho do declive gama demasiado pequeno: %i\n"

#: ../src/gamma-randr.c:266
#, c-format
msgid "Unable to restore CRTC %i\n"
msgstr "Incapaz de restaurar CRTC %i\n"

#: ../src/gamma-randr.c:290
msgid "Adjust gamma ramps with the X RANDR extension.\n"
msgstr "Ajustar o declive gama com a extensão X RANDR.\n"

#. TRANSLATORS: RANDR help output
#. left column must not be translated
#: ../src/gamma-randr.c:295
msgid ""
"  screen=N\t\tX screen to apply adjustments to\n"
"  crtc=N\tList of comma separated CRTCs to apply adjustments to\n"
msgstr ""

#: ../src/gamma-randr.c:317
#, c-format
msgid "Unable to read screen number: `%s'.\n"
msgstr ""

#: ../src/gamma-randr.c:353 ../src/gamma-vidmode.c:145
#, c-format
msgid ""
"Parameter `%s` is now always on;  Use the `%s` command-line option to "
"disable.\n"
msgstr ""

#: ../src/gamma-vidmode.c:50 ../src/gamma-vidmode.c:70
#: ../src/gamma-vidmode.c:79 ../src/gamma-vidmode.c:106
#: ../src/gamma-vidmode.c:169 ../src/gamma-vidmode.c:214
#, c-format
msgid "X request failed: %s\n"
msgstr "Pedido do X falhou: %s\n"

#: ../src/gamma-vidmode.c:129
msgid "Adjust gamma ramps with the X VidMode extension.\n"
msgstr "Ajustar o declive gama com a extensão X VidMode.\n"

#. TRANSLATORS: VidMode help output
#. left column must not be translated
#: ../src/gamma-vidmode.c:134
msgid "  screen=N\t\tX screen to apply adjustments to\n"
msgstr ""

#: ../src/gamma-dummy.c:32
msgid ""
"WARNING: Using dummy gamma method! Display will not be affected by this "
"gamma method.\n"
msgstr ""

#: ../src/gamma-dummy.c:49
msgid ""
"Does not affect the display but prints the color temperature to the "
"terminal.\n"
msgstr ""

#: ../src/gamma-dummy.c:64
#, c-format
msgid "Temperature: %i\n"
msgstr ""

#: ../src/location-geoclue2.c:49
#, c-format
msgid ""
"Access to the current location was denied!\n"
"Ensure location services are enabled and access by this program is "
"permitted.\n"
msgstr ""

#: ../src/location-geoclue2.c:95
#, c-format
msgid "Unable to obtain location: %s.\n"
msgstr ""

#: ../src/location-geoclue2.c:138
#, c-format
msgid "Unable to obtain GeoClue Manager: %s.\n"
msgstr ""

#: ../src/location-geoclue2.c:154
#, c-format
msgid "Unable to obtain GeoClue client path: %s.\n"
msgstr ""

#: ../src/location-geoclue2.c:176
#, c-format
msgid "Unable to obtain GeoClue Client: %s.\n"
msgstr ""

#: ../src/location-geoclue2.c:217
#, c-format
msgid "Unable to set distance threshold: %s.\n"
msgstr ""

#: ../src/location-geoclue2.c:241
#, c-format
msgid "Unable to start GeoClue client: %s.\n"
msgstr ""

#: ../src/location-geoclue2.c:380
msgid "GeoClue2 provider is not installed!"
msgstr ""

#: ../src/location-geoclue2.c:387
#, fuzzy
msgid "Failed to start GeoClue2 provider!"
msgstr "Falhou a iniciar o fornecedor %s.\n"

#: ../src/location-geoclue2.c:422
msgid "Use the location as discovered by a GeoClue2 provider.\n"
msgstr ""

#: ../src/location-geoclue2.c:431 ../src/location-manual.c:96
#, fuzzy
msgid "Unknown method parameter"
msgstr "Parâmetro do método desconhecido: '%s'.\n"

#: ../src/location-manual.c:49
#, fuzzy
msgid "Latitude and longitude must be set."
msgstr "Latitude e longitude devem ser definidas.\n"

#: ../src/location-manual.c:65
msgid "Specify location manually.\n"
msgstr "Especifique a localização manualmente.\n"

#. TRANSLATORS: Manual location help output
#. left column must not be translated
#: ../src/location-manual.c:70
msgid ""
"  lat=N\t\tLatitude\n"
"  lon=N\t\tLongitude\n"
msgstr ""
"  lat=N\t\tLatitude\n"
"  lon=N\t\tLongitude\n"

#: ../src/location-manual.c:73
msgid ""
"Both values are expected to be floating point numbers,\n"
"negative values representing west / south, respectively.\n"
msgstr ""
"Ambos os valores são esperados serem números de vírgula flutuante,\n"
"valores negativos representam Oeste / Sul, respetivamente.\n"

#: ../src/location-manual.c:87
msgid "Malformed argument.\n"
msgstr "Argumento malformado.\n"

#: ../src/gammastep_indicator/statusicon.py:66
msgid "Suspend for"
msgstr "Suspender por"

#: ../src/gammastep_indicator/statusicon.py:68
msgid "30 minutes"
msgstr "30 minutos"

#: ../src/gammastep_indicator/statusicon.py:69
msgid "1 hour"
msgstr "1 hora"

#: ../src/gammastep_indicator/statusicon.py:70
msgid "2 hours"
msgstr "2 horas"

#: ../src/gammastep_indicator/statusicon.py:71
#, fuzzy
msgid "4 hours"
msgstr "2 horas"

#: ../src/gammastep_indicator/statusicon.py:72
#, fuzzy
msgid "8 hours"
msgstr "2 horas"

#: ../src/gammastep_indicator/statusicon.py:81
msgid "Autostart"
msgstr "Iniciar automaticamente"

#: ../src/gammastep_indicator/statusicon.py:93
#: ../src/gammastep_indicator/statusicon.py:103
msgid "Info"
msgstr "Informação"

#: ../src/gammastep_indicator/statusicon.py:98
msgid "Quit"
msgstr "Sair"

#: ../src/gammastep_indicator/statusicon.py:136
msgid "Close"
msgstr "Fechar"

#: ../src/gammastep_indicator/statusicon.py:301
msgid "<b>Status:</b> {}"
msgstr ""

#: ../src/gammastep_indicator/statusicon.py:349
msgid "For help output, please run:"
msgstr ""

#, fuzzy, c-format
#~ msgid "Color temperature: %uK"
#~ msgstr "Temperatura da cor: %uK\n"

#, fuzzy, c-format
#~ msgid "Brightness: %.2f"
#~ msgstr "Brilho: %.2f\n"

#, fuzzy, c-format
#~ msgid "Solar elevations: day above %.1f, night below %.1f"
#~ msgstr "Elevações solares: dia acima %.1f, noite abaixo  %.1f\n"

#, fuzzy, c-format
#~ msgid "Temperatures: %dK at day, %dK at night"
#~ msgstr "Temperaturas: %dK de dia, %dK à noite\n"

#, fuzzy, c-format
#~ msgid "Temperature must be between %uK and %uK."
#~ msgstr "A temperatura deve estar entre %uK e %uK.\n"

#, fuzzy, c-format
#~ msgid "Brightness values must be between %.1f and %.1f."
#~ msgstr "Valores de brilho devem estar entre %.1f e %.1f.\n"

#, fuzzy, c-format
#~ msgid "Brightness: %.2f:%.2f"
#~ msgstr "Brilho: %.2f:%.2f\n"

#, fuzzy, c-format
#~ msgid "Gamma value must be between %.1f and %.1f."
#~ msgstr "O valor gama deve estar entre %.1f e %.1f.\n"

#, fuzzy, c-format
#~ msgid "Period: %s"
#~ msgstr "Período: %s\n"

#, fuzzy, c-format
#~ msgid "Period: %s (%.2f%% day)"
#~ msgstr "Período: %s(%.2f%% dia)\n"

#, fuzzy, c-format
#~ msgid "Initialization of %s failed."
#~ msgstr "O arranque de %s falhou.\n"

#, fuzzy, c-format
#~ msgid "Try `-l %s:help' for more information."
#~ msgstr "Tente `-l %s:help' para mais informação.\n"

#, fuzzy, c-format
#~ msgid "Try `-m %s:help' for more information."
#~ msgstr "Tente `-m %s:help' para mais informação.\n"

#, fuzzy, c-format
#~ msgid "Try -m %s:help' for more information."
#~ msgstr "Tente -m %s:help' para mais informação.\n"

#, fuzzy
#~ msgid "redshift"
#~ msgstr "Redshift"

#, c-format
#~ msgid "Location: %.2f %s, %.2f %s\n"
#~ msgstr "Localização: %.2fº%s,%.2fº%s\n"

#~ msgid "Unable to save current gamma ramp.\n"
#~ msgstr "Incapaz de guardar o declive gama atual.\n"

#~ msgid "Unable to open device context.\n"
#~ msgstr "Incapaz de abrir o contexto do dispositivo.\n"

#~ msgid "Display device does not support gamma ramps.\n"
#~ msgstr "O dispositivo de visualização não suporta os declives gama.\n"

#~ msgid "Unable to restore gamma ramps.\n"
#~ msgstr "Incapaz de restaurar os declives gama.\n"

#~ msgid "Unable to set gamma ramps.\n"
#~ msgstr "Incapaz de definir os declives gama.\n"
